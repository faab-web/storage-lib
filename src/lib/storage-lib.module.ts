import { NgModule, ModuleWithProviders } from '@angular/core';
import { StorageMoguleOptions } from './interface/storage-mogule-options';
import { STORAGE_OPTIONS_TOKEN } from './const/storage-options-token';

@NgModule({
  declarations: [],
  imports: [],
  exports: []
})
export class StorageLibModule {
  static forRoot(options?: StorageMoguleOptions): ModuleWithProviders {
    return {
      ngModule: StorageLibModule,
      providers: [
        {
          provide: STORAGE_OPTIONS_TOKEN,
          useValue: options,
        }
      ]
    };
  }
}
