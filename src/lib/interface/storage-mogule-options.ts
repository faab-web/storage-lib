import { EnvironmentEnum } from '@faab/base-lib';

export interface StorageMoguleOptions {
  environment_prefix: EnvironmentEnum;
  app_key: string;
}
