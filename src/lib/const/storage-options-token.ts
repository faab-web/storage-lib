import { StorageMoguleOptions } from './../interface/storage-mogule-options';
import { InjectionToken } from '@angular/core';

export const STORAGE_OPTIONS_TOKEN = new InjectionToken<StorageMoguleOptions>('STORAGE_OPTIONS_TOKEN');
