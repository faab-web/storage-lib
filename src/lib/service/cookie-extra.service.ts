import { Injectable, Inject } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class CookieExtraService extends CookieService {

  private PREFIX = 'SERIALIZE:';
  private MAIN_PREFIX = 'app_';

  set(name: string, value, expires?: number | Date, path?: string, domain?: string, secure?: boolean): void {
    if (value instanceof Object) {
      value = this.PREFIX + JSON.stringify(value);
    }

    name = this.MAIN_PREFIX + name;
    super.set(name, value, expires, path, domain, secure);
  }


  getObj<T>(name: string): T {
    const str = this.get(name);
    if (str.search(this.PREFIX) === 0) {
      JSON.parse(str.substring(this.PREFIX.length));
      return JSON.parse(str.substring(this.PREFIX.length)) as T;
    }
    return null;
  }

  get(name: string): string {
    name = this.MAIN_PREFIX + name;
    return super.get(name);
  }


  delete(name: string, path?: string, domain?: string): void {
    // FIX for cookie service => https://github.com/7leads/ngx-cookie-service/issues/5
    const path_string = (typeof path === 'string') ? 'path=' + path + ';' : '',
      domain_string = (typeof path === 'string') ? 'domain=' + domain + ';' : '';
    name = this.MAIN_PREFIX + name;
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;' + path_string + domain_string;
    super.delete(name, path, domain);
  }

  public setOptions(main_prefix: string) {
    if (typeof main_prefix !== 'string') {
      return;
    }
    this.MAIN_PREFIX = main_prefix;
  }
}
