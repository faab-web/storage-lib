import { CookieExtraService } from './cookie-extra.service';
import { Injectable, Inject } from '@angular/core';
import { StorageMoguleOptions } from './../interface/storage-mogule-options';
import { STORAGE_OPTIONS_TOKEN } from './../const/storage-options-token';
import { EnvironmentEnum } from '@faab/base-lib';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  public static DEVIDER = '_';

 /*  public static header_content_type = 'Content-Type';
  public static header_x_api_version = 'X-API-VERSION';
  public static header_app_key = 'X-OE-APP-KEY';
  public static header_app_prefix = 'X-OE-APP-PREFIX';
  public static header_x_ignore_interceptor = 'X-IGNORE-INTERCEPTOR';

  public static header_uapp_key = 'X-OE-UAPP-KEY';
  public static header_authentication = 'Authorization'; */

  private columns_suffix = '_Columns';
  public sessionID = this.guid();

  private conf_environment_prefix: EnvironmentEnum = EnvironmentEnum.LOCAL;

  private app_key: string;
  private prefix_public: string = '';
  private prefix_private: string = ''; // dependend from user

  constructor(
    @Inject(STORAGE_OPTIONS_TOKEN) private config: StorageMoguleOptions,
    private cookie: CookieExtraService
    ) {

      if (
        typeof config.environment_prefix === 'string'
        && typeof EnvironmentEnum[config.environment_prefix.toUpperCase()] === 'string'
      ) {
        this.conf_environment_prefix = config.environment_prefix;
      }
      this.app_key = (typeof config.app_key === 'string') ? config.app_key : 'app_';

      this.prefix_public = this.conf_environment_prefix + StorageService.DEVIDER + this.app_key + StorageService.DEVIDER;

      this.cookie.setOptions(this.prefix_public);
  }

  private writeSession(name: string, value: string) {
    name = this.prefix_public + name;
    sessionStorage.setItem(name, value);
  }

  private readSession(name: string): string {
    name = this.prefix_public + name;
    return sessionStorage.getItem(name);
  }

  public updatePrivateData(user_id: string): void {
    this.prefix_private = this.prefix_public + user_id + StorageService.DEVIDER;
  }

  public guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  private removeSession(name: string) {
    name = this.prefix_public + name;
    return sessionStorage.removeItem(name);
  }

  public setCookie(name: string, value: string): void {
    const full_name: string = this.prefix_public + name;
    this.cookie.delete(full_name);
    this.cookie.set(full_name, value, new Date().getDate() + 365 * 40, '/', '');
  }

  public getCookie(name: string): string {
    return this.cookie.get(this.prefix_public + name);
  }

  public setCookiePrivate(name: string, value: string): void {
    const full_name: string = this.prefix_private + name;
    this.cookie.delete(full_name);
    this.cookie.set(full_name, value, new Date().getDate() + 365 * 40, '/', '');
  }

  public getCookiePrivate(name: string): string {
    return this.cookie.get(this.prefix_private + name);
  }

  public getAppKey(): string {
    return this.app_key;
  }

  public getPrefixPublic(): string {
    return this.prefix_public;
  }

  /* getUapp(): string {
    return this.cookie.get(StorageService.header_uapp_key);
  }

  setUapp(uapp: string) {
    if (this.getUapp()) {
      return;
    }
    this.cookie.delete(StorageService.header_uapp_key);
    this.cookie.set(StorageService.header_uapp_key, uapp, new Date().getDate() + 365 * 40, '/', '');
  } */

  getOtp(): { otp: string, time_process: number } {
    const default_otp = {otp: '', time_process: 0};
    const data_json: string = this.cookie.get('otp');
    if (!data_json) {
      return default_otp;
    }
    let data: { otp: string, time_process: number };
    try {
      data = JSON.parse(data_json);
    } catch (error) {
      return default_otp;
    }
    return (!data || typeof data.otp !== 'string' || typeof data.time_process !== 'number') ? default_otp : data;
  }

  setOtp(data: { otp: string, time_process: number }) {
    if (!data || typeof data.otp !== 'string' || typeof data.time_process !== 'number') {
      data = {otp: '', time_process: 0};
    }
    let data_json: string = '';
    try {
      data_json = JSON.stringify(data);
    } catch (error) {
      data_json = '';
    }
    this.cookie.delete('otp');
    this.cookie.set('otp', data_json, new Date().getDate() + 365 * 40, '/', '');
  }

  /* getAuthKey(): string {
    let str = this.cookie.get(StorageService.header_authentication);
    if (str === null) {
      str = '';
    }
    return str;
  }

  setAuthKey(authorization: string) {
    if (authorization === null) {
      this.cookie.delete(StorageService.header_authentication, '/', '');
      return;
    }
    this.cookie.set(StorageService.header_authentication, authorization, 0, '/');
  } */

  setTableColumns(component_name: string, data: string[]) {
    if (!component_name || !data || !Array.isArray(data)) {
      return;
    }
    const cookie_name = this.table_columns_cookie_name(component_name);
    if (!cookie_name) {
      return;
    }
    this.cookie.delete(cookie_name);
    this.cookie.set(cookie_name, data, new Date().getDate() + 365 * 40, '/', '');
  }

  getTableColumns(component_name: string): string[] {
    const empty: string[] = [];
    if (!component_name) {
      return empty;
    }
    const cookie_name = this.table_columns_cookie_name(component_name);
    if (!cookie_name) {
      return empty;
    }
    const cookie_string: string =  this.cookie.get(cookie_name);
    if (!cookie_string) {
      return empty;
    }
    const columns = this.cookie.getObj(cookie_name);
    return (columns && Array.isArray(columns)) ? columns : empty;
  }

  private table_columns_cookie_name(component_name: string) {
    return component_name + this.columns_suffix;
  }

  logout() {
    this.clearLocalStorage();
    // this.setAuthKey(null);
  }

  clearLocalStorage() {
    localStorage.clear();
  }

  writeСache(key: string, object: any) {
    key = this.prefix_public + key;
    if (object === null || object === undefined) {
      localStorage.removeItem(key);
      return;
    }
    localStorage.setItem(key, object);
  }

  readCache(key: string) {
    key = this.prefix_public + key;
    return localStorage.getItem(key);
  }

  setAvailable(available: boolean) {
    this.writeСache('available', available);
  }

  getAvailable(): boolean {
    const available = this.readCache('available');
    if (available === null) {
      this.setAvailable(true);
      return true;
    }
    return available === 'true';
  }

}

