/*
 * Public API Surface of storage-lib
 */

export * from './lib/service/storage-lib.service';
export * from './lib/storage-lib.module';
export * from './lib/interface/storage-mogule-options';
